import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable , Subject} from 'rxjs';
import { FriendAdapter, Friend} from '../listAdapter';
import { Adapter } from '../adapter'

// import { Friend} from '../models/friend.model'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private hc:HttpClient, private adapter:FriendAdapter) { }

  index:number;

  createFormEnabled:boolean =true;
  getIndexValue()
  {
    return this.index
  }

  setIndexValue(index){
    this.index=index
  }

  // createdDataUpdate:any;
  // dataUpdateToNumbers(){
  //   return this.createdDataUpdate
  // }

  // dataAfterParsing:any;
  // dataFromLocalStorageForComponents():Observable<any>{
  //   this.
  // }



  subject= new Subject

  setRefreshData(isRefresh){
    console.log(this.subject)
    this.subject.next(isRefresh)
  }

  getRefreshData():Observable<any>
  {
    return this.subject.asObservable()
  }


  setInLocalStorge(data){
    return localStorage.setItem('phoneNumbers',data)
  }

  getFromLocalStorage(){
    return localStorage.getItem('phoneNumbers')
  }


  data1:Friend[]
  get()
  {
    this.data1=JSON.parse(localStorage.getItem('phoneNumbers'))
    return this.data1.map((response)=>{
      console.log(response)
      return this.adapter.adapt(response)
    })
    
  }
}
