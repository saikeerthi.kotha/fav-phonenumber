import { Injectable } from '@angular/core';
import { Adapter} from './adapter'
// import { Friend } from './models/friend.model';


export class Friend {
    firstName:string;
    lastName:string;
    gender:boolean;
    martialStatus:string;
    mobileorphone1:string;
    mobileorphone2:string;
    pnumber1:number;
    pnumber2:number;
    isFav:boolean;
    constructor() { }
}

@Injectable({
    providedIn:'root'
})
export class FriendAdapter implements Adapter<Friend>{
    adapt(list): Friend {
        console.log(list);
        let obj = new Friend();
        obj.firstName = list.firstName;
        obj.lastName = list.lastName;	
        obj.gender =list.gender;
        obj.martialStatus=list.martialStatus;
        obj.mobileorphone1 =list.mobileorphone1;
        obj.mobileorphone2= list.mobileorphone2;
        obj.pnumber1=list.pnumber1;
        obj.pnumber2=list.pnumber2;
        obj.isFav=list.isFav	
		return obj;
    }
}