import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder} from '@angular/forms';
import { FormArray , FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Friend } from 'src/app/models/Friend.model';
 
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  constructor(public dataService:DataService, private hc:HttpClient, private fb:FormBuilder,private router:Router) {}

  // userForm =this.fb.group({
  //   mobiles:this.fb.array([])
  // })

  // get mobiles(){
  //   return this.userForm.get('mobiles') as FormArray;
  // }

  // addNewMobile(){
  //   this.mobiles.push(this.fb.control(''))
  // }

  ngOnInit(): void {
    
  }

  dataFromLocalStorage:Friend[]=[]
  createdDataToLocalStorage:string;
  formData(form : NgForm){
    console.log(form.value)
    form.value.isFav=this.fav
    alert("User created successfully")
    //data to  numbers
    // this.dataService.createdDataUpdate=form.value
    // this.dataFromLocalStorage=JSON.parse(localStorage.getItem('phoneNumbers'))
    this.dataFromLocalStorage=JSON.parse(this.dataService.getFromLocalStorage()) //service
    console.log(this.dataFromLocalStorage)
    this.dataFromLocalStorage.push(form.value)
    console.log(this.dataFromLocalStorage)
    this.createdDataToLocalStorage=JSON.stringify(this.dataFromLocalStorage)
    // localStorage.setItem('phoneNumbers',this.createdDataToLocalStorage)
    this.dataService.setInLocalStorge(this.createdDataToLocalStorage) //service

    this.dataService.setRefreshData(true);



    // this.router.navigate(['./numbers'])
  }

  fav:boolean=true;
  statusfav(){
    this.fav=!this.fav
  }

  

  public phoneNumbers:any[]=[{
    pnumber2:''
  }]

  // inputFieldEnabled:boolean=false
  addPhoneNumber(){
    this.phoneNumbers.push({
      pnumber2:''
    })
    // this.inputFieldEnabled=true
  }

  removePhoneNumber(i){
    console.log(i)
    this.phoneNumbers.splice(i,1)
  }

}
