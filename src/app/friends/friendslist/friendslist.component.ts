import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Friend } from 'src/app/models/Friend.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-friendslist',
  templateUrl: './friendslist.component.html',
  styleUrls: ['./friendslist.component.css']
})
export class FriendslistComponent implements OnInit {

  constructor(private dataService:DataService, private hc:HttpClient, private router:Router) {
  }

 searchTerm:string;
 ngOnInit(): void {
   console.log(this.dataService.get())
  //  this.hc.get<Friend>("assets/friendsdata.json").subscribe(data=>{
  //    console.log(data)
  //    this.data=data
  //    this.dataToLocalStorage=JSON.stringify(this.data)
  //    localStorage.setItem('phoneNumbers',this.dataToLocalStorage)
  //   //  this.data.push(this.dataService.dataUpdateToNumbers())
     
  //  })
   this.data=this.getFriendsList();
   // alert(this.dataService.RefreshData())
   this.dataService.getRefreshData().subscribe((response)=>{
   console.log(response)
   if(response){
   this.data=this.getFriendsList()
   }
   })
 
 }

 getFriendsList(){
  //  return JSON.parse(localStorage.getItem('phoneNumbers')) 
  return JSON.parse(this.dataService.getFromLocalStorage())     //service
 }

 
 data:Friend[]=[]
 dataToLocalStorage:any;
 edit(index){
   this.dataService.index=index
   this.dataService.setRefreshData(true)
   
   // console.log(this.data[index])
   this.dataService.createFormEnabled=false
   // this.dataService.index= index
   // console.log(this.dataService.index)
 }

 displayCreateForm(){
   this.dataService.createFormEnabled=true;
   // this.data.push(JSON.parse(localStorage.getItem('createdData')))
  //  this.ngOnInit()
   
 }

 isFav:boolean;
 
 statusfav(j){
  
   this.data[j].isFav=!this.data[j].isFav
   this.dataToLocalStorage=JSON.stringify(this.data)
  //  this.data=this.dataToLocalStorage
  //  localStorage.setItem('phoneNumbers',this.data)

  this.dataService.setInLocalStorge(this.dataToLocalStorage)    //service
   // this.data=JSON.parse(localStorage.getItem('phoneNumbers'))
   console.log(this.dataService.index)
   this.dataService.setIndexValue(j);
   this.dataService.setRefreshData(true)
   
 }


}
