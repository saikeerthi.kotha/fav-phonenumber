import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { Friend } from '../../models/Friend.model'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(public dataService:DataService) { }

  dataFromLocalStorage:any;
  data:Friend
  dataFromNumbers:Friend;
  indexFromNumbers:number;
  fav:boolean
  ngOnInit(): void {
    this.data=JSON.parse(localStorage.getItem('phoneNumbers'))
   
    // this.indexFromNumbers=this.ds.createdFormDataToJsonFile()
    // console.log(this.indexFromNumbers)

    this.dataFromNumbers=this.indexData()
    this.dataService.getRefreshData().subscribe(response=>{
      console.log(response)
      if(response==true){

        this.dataFromNumbers=this.indexData()
        // this.dataFromNumbers=this.statusFav()
      }
      
    })
    
  }

  index:number;
  indexData(){
    this.data=JSON.parse(localStorage.getItem('phoneNumbers'))
    this.dataFromLocalStorage=this.data
    this.index=this.dataService.getIndexValue()
    console.log(this.index)
    return this.dataFromLocalStorage[this.index]
  }

  // dataFromLocal:any;
  // isFav:boolean;
  // statusFav(){
  //   // this.ngOnInit()
  //   this.index=this.ds.createdFormDataToJsonFile()
  //   console.log(this.index)
  //   this.dataFromLocal= this.dataFromLocalStorage[this.index]
  //   return this.dataFromLocal.isFav
  // }

  cancel(){
    this.dataService.createFormEnabled=true;
  }

  
  submitForm(formData :NgForm){
    console.log(formData.value)
   
    this.index=this.dataService.getIndexValue()
    console.log(this.index)
    formData.value.isFav=this.dataFromLocalStorage[this.index].isFav
    this.dataFromLocalStorage[this.index]=formData.value
    this.dataFromLocalStorage=JSON.stringify(this.dataFromLocalStorage)
    // localStorage.setItem('phoneNumbers',this.dataFromLocalStorage)
    this.data=this.dataFromLocalStorage
    this.dataService.setInLocalStorge(this.data)     //service
    // this.dataFromLocalStorage=JSON.parse(localStorage.getItem('phoneNumbers'))
    // this.dataFromLocalStorage=JSON.parse(this.dataService.getFromLocalStorage())    //service
    this.dataService.createFormEnabled=true;
    this.dataService.setRefreshData(true)
  }

  delete(){
    console.log(this.indexFromNumbers)
    this.dataFromLocalStorage.splice(this.indexFromNumbers,1)
    this.dataFromLocalStorage=JSON.stringify(this.dataFromLocalStorage)
    // localStorage.setItem('phoneNumbers',this.dataFromLocalStorage)
    this.data=this.dataFromLocalStorage
    this.dataService.setInLocalStorge(this.data)       //service
    // this.dataFromLocalStorage = JSON.parse(localStorage.getItem('phoneNumbers'))

    this.dataService.createFormEnabled=true;
    this.dataService.setRefreshData(true)


  }

  statusfav(){
    this.dataFromLocalStorage[this.index].isFav=!this.dataFromLocalStorage[this.index].isFav
    console.log(this.dataFromLocalStorage[this.index].isFav)
    this.dataFromLocalStorage=JSON.stringify(this.dataFromLocalStorage)
    // localStorage.setItem('phoneNumbers',this.dataFromLocalStorage)
    this.data=this.dataFromLocalStorage
    this.dataService.setInLocalStorge(this.data)     //service
    this.dataService.index=this.index
    this.dataService.setRefreshData(true)
  }

  phoneNumbers:any[]=[{
    pnumber2:''
  }]

  removePhoneNumber(index){
    console.log(index)
    this.phoneNumbers.splice(index,1)
  }
  addPhoneNumber(){
    this.phoneNumbers.push({
      pnumber2:''
    })
  }
}
