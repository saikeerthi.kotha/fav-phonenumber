import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(empObj:object[],searchTerm:string ): unknown {
    if(searchTerm==undefined){
      return empObj;
    }
    else{
      return empObj.filter(x=>
        x["firstname"].toLowerCase().indexOf(searchTerm.toLowerCase())!== -1
      )
    }
  }

}
