import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule} from '@angular/forms';
import { SearchPipe } from './pipes/search.pipe';
import { CreateComponent } from './friends/create/create.component';
import { EditComponent } from './friends/edit/edit.component';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search/search/search.component';
import { FriendslistComponent } from './friends/friendslist/friendslist.component'

@NgModule({
  declarations: [
    AppComponent,
    SearchPipe,
    CreateComponent,
    EditComponent,
    SearchComponent,
    FriendslistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
