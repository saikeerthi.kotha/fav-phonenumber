export interface Friend {
    firstName:string;
    lastName:string;
    gender:boolean;
    martialStatus:string;
    mobileorphone1:string;
    mobileorphone2:string;
    pnumber1:number;
    pnumber2:number;
    isFav:boolean;
}


